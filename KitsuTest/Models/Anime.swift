//
//  Anime.swift
//  KitsuTest
//
//  Anime Structure
//
//  Created by Alex Schmidt on 7/19/18.
//  Copyright © 2018 DigitalMediaSmart. All rights reserved.
//
import UIKit

struct Anime {
    var id                 : String
    var title              : String
    var synopsis           : String
    var popularityRank     : Int
    var ratingRank         : Int
    var posterImage        : String
    var coverImage         : String
    var episodeCount       : Int
    var episodeLength      : Int
 
    init(id:             String,
         title:          String,
         synopsis:       String,
         popularityRank: Int,
         ratingRank:     Int,
         episodeCount:   Int,
         episodeLength:  Int,
         posterImage:    String,
         coverImage:     String
        ){
        self.id             = id
        self.title          = title
        self.synopsis       = synopsis
        self.popularityRank = popularityRank
        self.episodeCount   = episodeCount
        self.episodeLength  = episodeLength
        self.posterImage    = posterImage
        self.coverImage     = coverImage
        self.ratingRank     = ratingRank
    }
}
