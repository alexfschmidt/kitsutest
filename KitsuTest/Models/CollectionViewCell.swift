//
//  CollectionViewCell.swift
//  KitsuTest
//
//  Created by Alex Schmidt on 7/20/18.
//  Copyright © 2018 DigitalMediaSmart. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var animeImage: UIImageView!
    @IBOutlet weak var animeName:  UILabel!
}
