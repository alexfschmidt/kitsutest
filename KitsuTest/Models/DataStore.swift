//
//  DataStore.swift
//  KitsuTest
//
//  Created by Alex Schmidt on 7/21/18.
//  Copyright © 2018 DigitalMediaSmart. All rights reserved.
//
import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher

final class DataStore {
    
    static let sharedInstance            = DataStore()
    var        animes         : [Anime]  = []
    var        filteredAnimes : [Anime]  = []
    var        titles         : [String] = []
    var        filteredTitles : [String] = []
 
    func urlFromFilteredName(name: String) -> String{
        var url = String()
        for anime in filteredAnimes{
            if(anime.title == name){
                url = anime.posterImage
                break
            }
        }
        return url
    }
    
    func idFromFilteredName(name: String) -> Int{
        var id    = 0
        var key   = 0
        for anime in filteredAnimes{
            if(anime.title == name){
                id = key
                break
            }
            key = key + 1
        }
        return id
    }
    /////////////////////////
    //letting kitsu server handle query load and get filtered results
    ////////////////////////
    func getFilteredAnimeDetailsRequest(filter: String, completion: @escaping () -> Void){
        let components = filter.components(separatedBy: " ")
        var url        = String()
        if(components.count == 1){
            url        = "https://kitsu.io/api/edge/anime?filter[text]="+filter
        }else{
            //handle filter strings line "cowboy bebop"
            var tailString = String()
            for component in components{
                tailString.append(component)
                tailString.append("%20")
            }
            url = "https://kitsu.io/api/edge/anime?filter[text]="+tailString 
        }
        print(url)
        filteredAnimes = []
        filteredTitles = []
        Alamofire.request(url).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                // Build Anime Structure
                for i in 0..<swiftyJsonVar["data"].count{
                    //////////////////////////
                    //check for JSON null make Int values 0
                    let episodeLength = !(swiftyJsonVar["data"][i]["attributes"]["episodeLength"].null == nil) ? 0 : Int(swiftyJsonVar["data"][i]["attributes"]["episodeLength"].rawString()!)!
                    let episodeCount = !(swiftyJsonVar["data"][i]["attributes"]["episodeCount"].null == nil) ? 0 : Int(swiftyJsonVar["data"][i]["attributes"]["episodeCount"].rawString()!)!
                    let popularityRank = !(swiftyJsonVar["data"][i]["attributes"]["popularityRank"].null == nil) ? 0 : Int(swiftyJsonVar["data"][i]["attributes"]["popularityRank"].rawString()!)!
                    let ratingRank = !(swiftyJsonVar["data"][i]["attributes"]["ratingRank"].null == nil) ? 0 : Int(swiftyJsonVar["data"][i]["attributes"]["ratingRank"].rawString()!)!
                    //////////////////////////
                    let anim = Anime(id: String(swiftyJsonVar["data"][i]["id"].rawString()!),
                                     title: String(swiftyJsonVar["data"][i]["attributes"]["canonicalTitle"].rawString()!),
                                     synopsis: String(swiftyJsonVar["data"][i]["attributes"]["synopsis"].rawString()!),
                                     popularityRank: popularityRank,
                                     ratingRank: ratingRank,
                                     episodeCount: episodeCount,
                                     episodeLength: episodeLength,
                                     posterImage:  String(swiftyJsonVar["data"][i]["attributes"]["posterImage"]["large"].rawString()!),
                                     coverImage: String(swiftyJsonVar["data"][i]["attributes"]["coverImage"]["tiny"].rawString()!)
                    )
                    self.filteredTitles.append(anim.title)
                    var tempArray = [String]()
                    tempArray.append(String(swiftyJsonVar["data"][i]["attributes"]["coverImage"]["tiny"].rawString()!))
                    tempArray.append(String(swiftyJsonVar["data"][i]["attributes"]["posterImage"]["tiny"].rawString()!))
                    self.prefetchImages(myArray: tempArray)// now prefetch cover & poster images to cache
                    self.filteredAnimes.append(anim)// add data to Filtered Anime Structure
                }
            }
            completion() // alert viewcontroller when done
        }
    }
    /////////////////////////
    //Query kitsu server with pagination 20/page using offset
    ////////////////////////
    func getAnimeDetailsRequest(offset: Int, completion: @escaping () -> Void){
        let url = "https://kitsu.io/api/edge/anime?page[limit]=20&page[offset]="+String(offset)
        Alamofire.request(url).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                // Build Anime Structure
                for i in 0..<swiftyJsonVar["data"].count{
                    //////////////////////////
                    //check for JSON null make Int values 0
                    let episodeLength = !(swiftyJsonVar["data"][i]["attributes"]["episodeLength"].null == nil) ? 0 : Int(swiftyJsonVar["data"][i]["attributes"]["episodeLength"].rawString()!)!
                    let episodeCount = !(swiftyJsonVar["data"][i]["attributes"]["episodeCount"].null == nil) ? 0 : Int(swiftyJsonVar["data"][i]["attributes"]["episodeCount"].rawString()!)!
                    let popularityRank = !(swiftyJsonVar["data"][i]["attributes"]["popularityRank"].null == nil) ? 0 : Int(swiftyJsonVar["data"][i]["attributes"]["popularityRank"].rawString()!)!
                    let ratingRank = !(swiftyJsonVar["data"][i]["attributes"]["ratingRank"].null == nil) ? 0 : Int(swiftyJsonVar["data"][i]["attributes"]["ratingRank"].rawString()!)!
                    //////////////////////////
                    let anim = Anime(id: String(swiftyJsonVar["data"][i]["id"].rawString()!),
                                     title: String(swiftyJsonVar["data"][i]["attributes"]["canonicalTitle"].rawString()!),
                                     synopsis: String(swiftyJsonVar["data"][i]["attributes"]["synopsis"].rawString()!),
                                     popularityRank: popularityRank,
                                     ratingRank: ratingRank,
                                     episodeCount: episodeCount,
                                     episodeLength: episodeLength,
                                     posterImage:  String(swiftyJsonVar["data"][i]["attributes"]["posterImage"]["large"].rawString()!),
                                     coverImage: String(swiftyJsonVar["data"][i]["attributes"]["coverImage"]["tiny"].rawString()!)
                                    )
                    self.titles.append(anim.title)
                    var tempArray = [String]()
                    tempArray.append(String(swiftyJsonVar["data"][i]["attributes"]["coverImage"]["tiny"].rawString()!))
                    tempArray.append(String(swiftyJsonVar["data"][i]["attributes"]["posterImage"]["tiny"].rawString()!))
                    self.prefetchImages(myArray: tempArray)// now prefetch cover & poster images to cache
                    self.animes.append(anim) // add data to Anime Structure
                }
            }
            completion() // alert viewcontroller when done
        }
    }
    /////////////////////////
    //KingFisher way of prefetching URL images into cache
    ////////////////////////
    func prefetchImages(myArray: [String]){
        let urls       = myArray.map { URL(string: $0)! }
        let prefetcher = ImagePrefetcher(urls: urls) {
            skippedResources, failedResources, completedResources in
            //print("These resources are prefetched: \(completedResources)")
        }
        prefetcher.start()
    }
}
