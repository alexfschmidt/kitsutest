//
//  AnimeNavigationController.swift
//  KitsuTest
//
//  Created by Alex Schmidt on 7/23/18.
//  Copyright © 2018 DigitalMediaSmart. All rights reserved.
//

import UIKit

class AnimeNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .lightContent
    }
}
