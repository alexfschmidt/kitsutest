//
//  ViewController.swift
//  KitsuTest
//
//  Created by Alex Schmidt on 7/19/18.
//  Copyright © 2018 DigitalMediaSmart. All rights reserved.
//
import UIKit
import Alamofire
import SwiftyJSON

class AnimeController: UICollectionViewController,
                       UICollectionViewDelegateFlowLayout,
                       UISearchControllerDelegate,
                       UISearchBarDelegate,
                       UISearchResultsUpdating  {
    //////////////////////////////////////////////////////
    //System Variables
    //////////////////////////////////////////////////////
    let dataStore           = DataStore.sharedInstance
    var selectedID          = 0
    var animeCount          = 0
    var offset              = 0
    var searchActive : Bool = false
    let searchController    = UISearchController(searchResultsController: nil)
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        ////////////////////////////
        // Set collection view layout params
        ////////////////////////////
        let layout                     = self.collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset            = UIEdgeInsetsMake(0, 5, 0, 5)
        layout.minimumInteritemSpacing = 5
        layout.headerReferenceSize     = CGSize(width: 50, height: 50)
        ////////////////////////////
        // Add SearchController to main view
        ////////////////////////////
        self.searchController.searchResultsUpdater                 = self
        self.searchController.delegate                             = self
        self.searchController.searchBar.delegate                   = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation     = true
        self.searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder                     = "Search Animes"
        searchController.searchBar.sizeToFit()
        searchController.searchBar.becomeFirstResponder()
        //add search bar to navigation area programatically
        self.navigationItem.titleView                              = searchController.searchBar
        ////////////////////////////
        // Get Anime Data and reload collection
        ////////////////////////////
        self.getAnimeIDs {
            //fetch anime by pagination (limit / page 20)
            self.dataStore.getAnimeDetailsRequest(offset: self.offset) {
                self.collectionView?.reloadData()
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////
    //Query Anime API Functions
    ////////////////////////////////////////////////////////////////////////
    func getAnimeIDs(completion: @escaping () -> Void) {
        Alamofire.request("https://kitsu.io/api/edge/anime").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                //anime max count
                self.animeCount = Int(swiftyJsonVar["meta"]["count"].rawString()!)!
                completion() // alert viewcontroller when done
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////
    //UISearch Functions
    ////////////////////////////////////////////////////////////////////////
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.collectionView?.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateSearchResults(for searchController: UISearchController){
        let searchString = searchController.searchBar.text
        //get filtered anime list and load into collection
        self.dataStore.getFilteredAnimeDetailsRequest(filter: searchString!){
            print("searchString = \(String(describing: searchString))  filtered anime count \(self.dataStore.filteredAnimes.count)")
            self.collectionView?.reloadData()
        } 
        /*filtered         = dataStore.titles.filter({ (filter) -> Bool in
        let filteredText: NSString = filter as NSString
            return (filteredText.range(of: searchString!, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })*/
    }
    
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        self.collectionView?.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.collectionView?.reloadData()
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        if !searchActive {
            searchActive = true
            self.collectionView?.reloadData()
        }
        searchController.searchBar.resignFirstResponder()
    }
    ////////////////////////////////////////////////////////////////////////
    //CollectionView Funtions
    ////////////////////////////////////////////////////////////////////////
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searchActive {
            return self.dataStore.filteredAnimes.count
        }else{
            return dataStore.animes.count
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell                   = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        if searchActive{
            //if searching get URL and Title from filtered array
            let url                    = URL(string: dataStore.urlFromFilteredName(name: self.dataStore.filteredTitles[indexPath.row]))
            cell.animeName.text        =  self.dataStore.filteredTitles[indexPath.row]
            cell.animeImage.kf.setImage(with: url)//load cached image
        }else{
            let data                   = dataStore.animes[indexPath.row]
            let url                    = URL(string: data.coverImage)
            cell.animeName.text        = data.title
            cell.animeImage.kf.setImage(with: url)//load cached image
        }
        cell.layer.borderColor     = UIColor.lightGray.cgColor
        cell.layer.borderWidth     = 0.8
        cell.layer.backgroundColor = UIColor.clear.cgColor
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if searchActive{
            selectedID = dataStore.idFromFilteredName(name: self.dataStore.filteredTitles[indexPath.item])
        }else{
            selectedID = indexPath.item
        }
        performSegue(withIdentifier: "animeDetailsSegue", sender: nil)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !searchActive{
            //only add to collectionview when not in search mode
            if (scrollView.contentOffset.y ==
                (scrollView.contentSize.height - scrollView.frame.size.height)
               ){
                //detect when scroll is at bottom and load another 20 anime
                offset = (offset + 20 ) > animeCount ? offset : offset + 20
                self.dataStore.getAnimeDetailsRequest(offset: self.offset) {
                    self.collectionView?.reloadData()
                }
            }
        }
    }
    /////////////////////////////////////////////////////
    //Segues Event  pass data to other controller
    /////////////////////////////////////////////////////
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "animeDetailsSegue" {
            if let destinationVC = segue.destination as? AnimeDetailsController {
                if searchActive{
                    destinationVC.anime = dataStore.filteredAnimes
                }else{
                    destinationVC.anime = dataStore.animes
                }
                destinationVC.selectedID = self.selectedID
            }
        }
    }
    /////////////////////////////////////////////////////
    //Misc
    /////////////////////////////////////////////////////
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
