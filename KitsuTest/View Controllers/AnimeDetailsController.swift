//
//  AnimeDetails.swift
//  KitsuTest
//
//  Created by Alex Schmidt on 7/20/18.
//  Copyright © 2018 DigitalMediaSmart. All rights reserved.
//
import UIKit
import SwiftyJSON
import Alamofire

class AnimeDetailsController: UIViewController {
    
    var anime       : [Anime]   = []
    var selectedID              = 0
    
    @IBOutlet weak var animeImage        : UIImageView!
    @IBOutlet weak var animeTitle        : UILabel!
    @IBOutlet weak var popularityRanking : UILabel!
    @IBOutlet weak var ratingRank        : UILabel!
    @IBOutlet weak var episodeCount      : UILabel!
    @IBOutlet weak var episodeLength     : UILabel!
    @IBOutlet weak var synopsis          : UITextView!
    @IBOutlet weak var synopsisFrameView : UIView!
    @IBOutlet weak var detailsFrameView  : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //improve look of image window
        animeImage.layer.cornerRadius         = 10.0
        animeImage.layer.masksToBounds        = true
        animeImage.layer.borderColor          = UIColor.darkGray.cgColor
        animeImage.layer.borderWidth          = 1.0
        synopsisFrameView.layer.cornerRadius  = 10.0
        synopsisFrameView.layer.masksToBounds = true
        synopsisFrameView.layer.borderColor   = UIColor.darkGray.cgColor
        synopsisFrameView.layer.borderWidth   = 1.0
        detailsFrameView.layer.cornerRadius   = 10.0
        detailsFrameView.layer.masksToBounds  = true
        detailsFrameView.layer.borderColor    = UIColor.darkGray.cgColor
        detailsFrameView.layer.borderWidth    = 1.0
        /////////////////////////////////////////////
        //Display anime information
        /////////////////////////////////////////////
        self.showAnimaInfo()
    }
    
    func showAnimaInfo(){
        let anim               = anime[selectedID] //get element from Anime Structure
        let url                = URL(string: anim.posterImage)
        animeTitle.text        = anim.title
        synopsis.text          = anim.synopsis
        popularityRanking.text = String(anim.popularityRank)
        ratingRank.text        = String(anim.ratingRank)
        episodeCount.text      = String(anim.episodeCount)
        episodeLength.text     = String(anim.episodeLength)
        animeImage.kf.setImage(with: url) //get image from cached data previously prefetched at start
    }
}
